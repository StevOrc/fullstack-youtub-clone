import bcrypt from 'bcryptjs';
import jwt from 'jsonwebtoken';
import User from '../models/Users.model.js';
import { JWT_SECRET_KEY } from '../helpers/env-variables.js';
import { createError } from '../helpers/create-error.js';

export const signup = async (req, res, next) => {
    try {
        const { password } = req.body;
        const salt = bcrypt.genSaltSync(10);
        const hash = bcrypt.hashSync(password, salt);
        const newUser = new User({ ...req.body, password: hash });
        await newUser.save();
        return res.status(200).json('New user has been created');
    } catch (error) {
        next(error);
    }
};

export const signin = async (req, res, next) => {
    try {
        const { email, password } = req.body;

        const user = await User.findOne({ email });
        if (!user) return next(createError(404, `User not found with the given name ${email}`));

        const isCorrect = await bcrypt.compare(password, user.password);
        if (!isCorrect) return next(createError(404, 'Wrong credentials'));

        const token = jwt.sign({ id: user._id }, JWT_SECRET_KEY);

        const { password: userPassword, __v, ...others } = user._doc;

        res.cookie('access_token', token, { httpOnly: true }).status(200).json(others);
    } catch (error) {
        next(error);
    }
};
