import User from "../models/Users.model.js";

export async function getAllUsers(req, res, next) {
  try {
    const users = await User.find();
    return res.status(200).send({ message: "get all users" });
  } catch (error) {
    next(error);
  }
}
