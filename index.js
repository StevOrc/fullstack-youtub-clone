import express from 'express';
import cors from 'cors';
import cookieParser from 'cookie-parser';
import { PORT } from './helpers/env-variables.js';
import errorHandler from './middlewares/error-handler.js';
import routes from './routes/index.js';
import { initDb } from './helpers/db.js';
import { createError } from './helpers/create-error.js';

const app = express();
app.use(cookieParser());
app.use(cors());
app.use(express.json());

app.use('/api/v1', routes);

app.use('*', () => {
    throw createError(404, 'Route not found');
});

app.use(errorHandler);

initDb();

app.listen(PORT, () => {
    console.log(`Listening on port ${PORT}`);
});
