const errorHandler = (err, req, res, next) => {
    if (err) {
        return res.status(err.status || 500).send({ message: err.message });
    }

    next();
};

export default errorHandler;
