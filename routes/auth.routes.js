import { Router } from "express";
import * as authController from "../controllers/auth.controller.js";

const router = Router();

// CREATE USER
router.post("/signup", authController.signup);
// SIGNING USER
router.post("/signin", authController.signin);
// GOOGLE AUTH
router.post("/google");

export default router;
