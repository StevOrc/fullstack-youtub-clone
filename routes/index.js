import { Router } from "express";
import userRoutes from "./user.routes.js";
import authRoutes from "./auth.routes.js";
import videoRoutes from "./videos.routes.js";
import commentRoutes from "./comments.routes.js";

const router = Router();

router.use("/comments", commentRoutes);
router.use("/videos", videoRoutes);
router.use("/users", userRoutes);
router.use("/auth", authRoutes);

export default router;
