import { Router } from "express";
import * as videoController from "../controllers/videos.controller.js";

const router = Router();

router.get("/", videoController.getAllVideos);

export default router;
